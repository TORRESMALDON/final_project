# final_project

This repo contains the files used to complete the final project for AOS573. In here you will find the directory with the yearly files for AEW tracks and a notebook with the proyect code.

DATA: 
ym_matched_tracks

This data is based on the AEW tracking methods of Brammer et al. (2018) and was created combining NCEP CFSR v2 and ECMWF interim reanalysis (ERA-interim). The data was also used and modified by Dr. Kelly Nuñez Ocasio (2021) which conducted a very similar project using AEW tracks. These files were obtained directly from Dr. Nuñez Ocasio. The data consists of yearly files of AEW tracks from 1979 to 2017 with six-hourly data for the months of June through October.


CODE:
AEWsTracks.ipynb

The code used for this project presents a process of how to clean and prepare the data that I will be using for my current graduate research project. The main goal of this code is to make the data more accessible and usable by putting all the data files in one place, separating and cleaning the data, and subselecting the most relevant parameters.

This code is just the first step of my current research, and is used as an opportunity to move my graduate research forward. With this code we could eventually analyze the development of African Easterly Waves (AEWs) into tropical cyclones (TCs). The code mostly consist of separating the tracks into two categories: events that developed (DAEWs), and events that did not develop (NDAEWs).This step is the most important and it will be tha main focus of this project. Once the tracks are separated they can be used to perform the relative analyses of the research project.